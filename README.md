A simple shopping app by Jenn (jslee0725@hotmail.com).

Author: Jenn
Date: 2018-07-17
This is the first time i use JIRA, bitbucket and Yii2 framework.

Question URL: https://www.hermo.my/hermo-x-challenge/logic

Simple shopping app consists of 3 steps:
1. Product Listing
2. Checkout
3. Summary

Product Listing
---------------
- Display several products with price range (MYR25-MYR200)
- Choose a product and quantity to checkout

Checkout
--------
- Display selected product
- Choose ship to country to get the shipping fee
- Allowed to input promotion code to get discount

Summary
-------
- Display the product, delivery, payment summary