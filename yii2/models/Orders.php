<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $product_id
 * @property int $quantity
 * @property int $delivery_country
 * @property string $discount_code
 * @property string $unit_price
 * @property string $total_price
 *
 * @property Products $product
 */
class Orders extends \yii\db\ActiveRecord
{
	private static $countryMatrix = [
		0 => 'Malaysia',
		1 => 'Singapore',
		2 => 'Brunei'
	];
	
	
	public $priceInfo;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'quantity', 'delivery_country', 'unit_price', 'total_price'], 'required'],
            [['product_id', 'delivery_country'], 'integer'],
			[['quantity'],'integer','min' => 1, 'max' => 10],
            [['unit_price', 'total_price'], 'number'],
            [['discount_code'], 'string', 'max' => 15],
			[['discount_code'], 'validateDiscountCode'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'delivery_country' => 'Delivery Country',
            'discount_code' => 'Discount Code',
            'unit_price' => 'Unit Price',
            'total_price' => 'Total Price',
        ];
    }

    /*
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
	
	/*
	 * return countryName
	*/
	public function getCountryName()
	{
		return isset(self::$countryMatrix[$this->delivery_country]) ? self::$countryMatrix[$this->delivery_country] : null;

	}
	
	/*
	 * return countryMatrix
	*/
	public function getCountryMatrix()
	{
		return self::$countryMatrix;
	}
	
	
	
	public function validateDiscountCode($attribute, $params,$validator)
    {
		
		switch ($this->$attribute)
		{
			case 'OFF5PC':
				if ($this->quantity < 2)
				{
					$this->addError($attribute,'Minimum purchase quantity is 2 to use this discount code.');
				}
				break;
			case 'GIVEME15':
				if (($this->quantity * $this->product->selling_price) < 100)
				{
					$this->addError($attribute,'Minimum purchase is MYR100 to use this discount code.');
				}
				break;
			default:
			case 'ERROR';
				$this->addError($attribute,'Invalid Discount Code');
				break;
		}
		


    }
}
