<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $image
 * @property string $brand
 * @property string $product_name
 * @property string $retail_price
 * @property string $selling_price
 *
 * @property Orders[] $orders
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image', 'brand', 'product_name', 'retail_price', 'selling_price'], 'required'],
            [['retail_price', 'selling_price'], 'number'],
            [['image', 'product_name'], 'string', 'max' => 255],
            [['brand'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'brand' => 'Brand',
            'product_name' => 'Product Name',
            'retail_price' => 'Retail Price',
            'selling_price' => 'Selling Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['product_id' => 'id']);
    }
	
	/**
     * @return All Products
     */
	public function getAll()
	{
		return $this->find()->all();
	}
}
