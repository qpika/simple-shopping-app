<?php

	use app\widgets\Alert;
	use yii\helpers\Html;
	//use yii\bootstrap\Nav;
	//use yii\bootstrap\NavBar;
	//use yii\widgets\Breadcrumbs;
	use app\assets\AppAsset;

	AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
	<style>
		body,html	{
			width:100%;
			height:100%;
			min-height:100%;
		}
	</style>
</head>
	
<body>
	<div class="w3-card-4">
		<div id="jHeader" class="w3-container w3-theme">
			<h2><?= Html::encode($this->title) ?></h2>
		</div>
	</div>
<?php $this->beginBody() ?>
	<div id="jBody">
		<?php if (Yii::$app->session->hasFlash('error')): ?>
			<div class="w3-container w3-animate-right w3-display-container w3-red">
			  <span onclick="this.parentElement.style.display='none'"
			  class="w3-button w3-display-topright">&times;</span>
			  <h3>Error!</h3>
			  <p><?= Yii::$app->session->getFlash('error') ?></p>
			</div> 
		<?php endif; ?>
	<?= $content ?>
	</div>
	
<?php $this->endBody() ?>
	<div id="jFooter" class="w3-container w3-theme-d5">
		<div class="w3-small">
			<p>By Jenn @<?=Date('Y')?></p>
		</div>
	</div>
	
</body>
</html>
<?php $this->endPage() ?>