<?php

/* @var $this yii\web\View */
/*
	@var model Products model
	@var products Products::getAll
	@var title PageTitle
*/
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = $title;


?>
<div class="w3-container">
	<div class="w3-panel"><h3>Product Listing</h3></div>
	<?php if (isset($products)) {?>
    <div class="w3-section">
		<div class="w3-row">
       <?php
			foreach($products as $product){
			   echo $this->render('product_card',['product'=>$product]);
		   }
	   ?>
	   </div>
    </div>
	<?=$this->render('modal_window',['modalId'=>'jPurchase','modalHeader'=>'Purchase Form'])?>
	
	<?php }	else { ?>
	<div>
		<p>Nothing to show.</p>
	</div>
	<?php } ?>
</div>
