<?php


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
$formatter = \Yii::$app->formatter;
$this->title = $title;
/*Use customized ajaxValidate JS*/
$urlTotalByAjax = Url::toRoute('shop/ajax-get-total-price');
$this->registerJs( <<< EOT_JS


$("#orders-delivery_country").on('change.yii',validateAjax);
$("#orders-discount_code").on('blur',validateAjax);

$('#check-out-form').on('beforeSubmit', function (e) {
	if ($('#orders-discount_code').val() === '')
	{
		console.log(validateAjax);
		if (!confirm("Without Discount Code?")) {
			return false;
		}
		
	}


    return true;
});



function validateAjax()
{

	var country = $('#orders-delivery_country').val();
	var discount_code = $('#orders-discount_code').val();
	var param = {
		'Orders' : {
			'delivery_country' : country,
			'discount_code' : discount_code
			}
		};
	

	$.post(
		'{$urlTotalByAjax}',
		param,
		function(data) {
			//console.log(data);
			 			
			var priceInfo = data.Price;
			$('td#priceInfo-discount').html(priceInfo.Discount).addClass('w3-animate-left').delay(1000).queue(function(){
				$(this).removeClass("w3-animate-left").dequeue();
			});
			$('td#priceInfo-shipfee').html(priceInfo.ShipFee).addClass('w3-animate-left').delay(1000).queue(function(){
				$(this).removeClass("w3-animate-left").dequeue();
			});;
			$('td#priceInfo-total').html(priceInfo.Total).addClass('w3-animate-left').delay(1000).queue(function(){
				$(this).removeClass("w3-animate-left").dequeue();
			});;
			
			if(data.Error) {
				$('#check-out-form').yiiActiveForm('updateMessages', data.Error, true);
            }

			
		}
	)

}

EOT_JS

);

?>
<div class="w3-container">
	<div class="w3-container"><h3>Check Out</h3></div>
	<div class="w3-row w3-small w3-section">
		<div class="w3-third">
			<div class="w3-large w3-padding w3-theme-d3">Product Info</div>
			<div class="w3-circle w3-green w3-display-container" style="width:100px;height:100px;margin: 16px auto">
				<span class="w3-display-middle">IMG</span>
			</div>
			<div class="w3-panel w3-center"><p><strong><?=Html::encode($model->product->product_name)?></strong> (<?=$model->product->brand?>)</p></div>
		</div>
		<div class="w3-third">
			<div class="w3-large w3-padding w3-theme-d3">Price Info</div>
			<div class="w3-panel">
				<table class="w3-table" style="font-weight:bold">
				<tr>
				  <td><strong>Price</strong></td><td class="w3-right-align"><span class="w3-text-grey w3-tiny" style="text-decoration:line-through ">(<?=Html::encode($formatter->asCurrency($model->product->retail_price))?>)</span> <?=Html::encode($formatter->asCurrency($model->product->selling_price))?></td>
				</tr>
				<tr>
				  <td><strong>Quantity</strong></td><td class="w3-right-align"><?=Html::encode($model->quantity)?></td>
				</tr>
				<tr>
				  <td><strong>Subtotal</strong></td><td class="w3-right-align"><?=Html::encode($formatter->asCurrency($model->priceInfo['ItemOnly']))?></td>
				</tr>
				<tr>
				  <td><strong>Discount</strong></td><td id="priceInfo-discount" class="w3-right-align">- <?=Html::encode($formatter->asCurrency($model->priceInfo['Discount']))?></td>
				</tr>
				<tr>
				  <td><strong>Shipping Fee</strong></td><td id="priceInfo-shipfee" class="w3-right-align"><?=Html::encode($formatter->asCurrency($model->priceInfo['ShipFee']))?></td>
				</tr>
				<tr>
				  <td><strong>Total</strong></td><td id="priceInfo-total" class="w3-right-align"><?=Html::encode($formatter->asCurrency($model->priceInfo['Total']))?></td>
				</tr>
				</table>
			</div>
		</div>
		<?php 		
			$form = ActiveForm::begin(
				[
					'id' => 'check-out-form',
					'enableClientValidation' => true,
					'validateOnChange' => true,
					'validateOnSubmit' => true,
					'action' => Url::toRoute('shop/check-out'),
					'options' => ['class'=>'w3-third'],
					'errorCssClass' => 'w3-text-red'
				]
			);
		?>
		<div class="w3-large w3-padding w3-theme-d3">Extra Info</div>
		<div class="w3-panel">
			<?=$form->field($model,'id')->hiddenInput()->label(false)?>
			<?=$form->field($model,'product_id')->hiddenInput()->label(false)?>
			<?=$form->field($model,'quantity')->hiddenInput()->label(false)?>
			<?=$form->field($model,'discount_code')->textInput(['class'=>'w3-input w3-border','placeholder' => 'OFF5PC / GIVEME15','autofocus' => true])?>
			<?=$form->field($model,'delivery_country')->dropDownList($model->countryMatrix,['prompt'=>'-Select a country-','class'=>'w3-select w3-border'])?>

			<?=Html::a('Cancel',Url::toRoute('/'),['class'=>'w3-btn w3-theme-action'])?>
			<?=Html::submitButton('Check Out',['class'=>'w3-btn w3-theme-action w3-right'])?>

			<?php ActiveForm::end();?>
		</div>
	</div>
</div>