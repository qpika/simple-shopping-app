<?php


use yii\helpers\Html;
//use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
$formatter = \Yii::$app->formatter;
$this->title = $title;
//var_dump($myorder);
?>

<div class="w3-container">
	<div class="w3-container w3-border-bottom"><ul class="w3-ul">
		<li><h3>Order Number : <?=Html::encode($myorder['orderNumber'])?></h3></li></ul>
	</div>
	<div class="w3-container w3-section">
		<div class="w3-third w3-center">
			<div class="w3-circle w3-green w3-display-container" style="width:100px;height:100px;margin: 16px auto">
				<span class="w3-display-middle">IMG</span>
			</div>
			<p><strong><?=Html::encode($myorder['product_idName'])?></strong></p>
			<p><?=Html::encode($myorder['brand'])?></p>
		</div>
		<div class="w3-quarter">
			<div class="panel">
				<table class="w3-table w3-small">
					<tr>
					  <td><strong>Unit Price</strong></td><td class="w3-right-align"><?=Html::encode($formatter->asCurrency($myorder['unit_price']))?></td>
					</tr>
					<tr>
					  <td><strong>Quantity</strong></td><td class="w3-right-align"><?=Html::encode($myorder['quantity'])?></td>
					</tr>
					<tr class="w3-border-top">
					  <td><strong>Subtotal</strong></td><td class="w3-right-align"><?=Html::encode($formatter->asCurrency($myorder['priceInfo']['ItemOnly']))?></td>
					</tr>
					<tr>
					  <td><strong>Discount Code</strong></td><td class="w3-right-align"><?=Html::encode($myorder['discount_code'])?></td>
					</tr>
					<tr>
					  <td><strong>Discount</strong></td><td class="w3-right-align">- <?=Html::encode($formatter->asCurrency($myorder['priceInfo']['Discount']))?></td>
					</tr>
					<tr>
					  <td><strong>Delivery Country</strong></td><td class="w3-right-align"><?=Html::encode($myorder['delivery_countryName'])?></td>
					</tr>
					<tr>
					  <td><strong>Shipping Fee</strong></td><td class="w3-right-align"><?=Html::encode($formatter->asCurrency($myorder['priceInfo']['ShipFee']))?></td>
					</tr>
					<tr class="w3-border-top">
					  <td><strong>Total</strong></td><td class="w3-right-align"><?=Html::encode($formatter->asCurrency($myorder['priceInfo']['Total']))?></td>
					</tr>
				</table>
			</div>
			<p><?=Html::a('Finish',Url::toRoute('/'),['class'=>'w3-btn w3-theme-action w3-right'])?></p
		</div>
	
	</div>
</div>