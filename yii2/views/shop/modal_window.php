<?php
/**
*Modal template for purchase form
**/
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div id="<?=Html::encode($modalId)?>" class="w3-modal">
  <div class="w3-modal-content w3-animate-zoom w3-card-4">
	
    <header class="w3-container w3-theme-d5"> 
      <span onclick="closeMe('jPurchase')" class="w3-button w3-display-topright">&times;</span>
      <h2><?=Html::encode($modalHeader)?></h2>
    </header>
    <div id="modal-content" class="w3-container">
		<p><i class="fa fa-spinner w3-spin" style="font-size:48px"></i></p>
    </div>

    <footer class="w3-container">
	</footer>
  </div>
</div>