<?php
/*
 * Display 1 single product purchase form
 * @var $model = Selected Product
*/

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
$formatter = \Yii::$app->formatter;
$dropDownValues = range(1,10);
foreach($dropDownValues as $value)	{
	$dropDownData[$value] = $value;
};
?>

<?php 		
	$form = ActiveForm::begin(
		[
			'id' => 'product-form',
			'action' => Url::toRoute('shop/index')
		]
	);
	
?>
<?=$form->field($model,'id')->hiddenInput()->label(false)?>
<?=$form->field($model,'product_id')->hiddenInput()->label(false)?>
<div class="w3-section">
	<p><?=Html::encode($model->product->product_name)?></p>
	<p><?=$formatter->asCurrency($model->product->selling_price)?></p>
	<div class="w3-row">
		<?=$form->field($model,'quantity')->dropDownList($dropDownData,['class'=>'w3-select w3-border'])?>
	</div>
</div>


	<p><?=Html::submitButton('Buy',['class' => 'w3-btn w3-theme-action'])?></p>



<?php ActiveForm::end();?>