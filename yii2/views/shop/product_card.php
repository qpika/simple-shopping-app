<?php
/**
* Display Product Card
**/
use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\ActiveForm;
$formatter = \Yii::$app->formatter;

?>

<div class="w3-third w3-padding">
	<div class="w3-hover-shadow w3-center w3-theme-l5" style="min-height:180px">
		<header class="w3-theme-d2">
			<span class="w3-padding w3-large"><?=$product['brand']?></span>
		</header>
		<div class="w3-row-padding w3-center">
			<div class="w3-third w3-mobile">
				<div class="w3-circle w3-green w3-display-container w3-center" style="width:100px;height:100px;margin:16px auto">
					<span class="w3-display-middle">IMG</span>
				</div>
			</div>
			<div class="w3-twothird w3-mobile">
				<div class="w3-container w3-small w3-margin">
					<div class="w3-row">
						<h5><?=$product['product_name']?></h5>
					</div>
					<div class="w3-row">
						<span class="w3-text-grey" style="text-decoration:line-through"><?=$formatter->asCurrency($product['retail_price'])?></span><br/><span><strong><?=$formatter->asCurrency($product['selling_price'])?></strong></span>
					</div>
					<div class="w3-row">
						<?=HTML::button('Buy Now',[
							'class'=>'w3-btn w3-theme-action w3-margin-top',
							'onclick'=>'openModal(\'jPurchase\',\''.$product['id'].'\',\''.Url::toRoute('shop/show-product/'.$product['id']).'\')'])?>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

