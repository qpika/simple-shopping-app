<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Products;
use app\models\Orders;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\base\Security;


class ShopController extends Controller
{
	
	/**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    /**
     * Displays shopping app 1st page.
     *
     * @return string
     */
    public function actionIndex()
    {
		$session = Yii::$app->Session;
		/*Clear session if back to index page*/
		/*If got flash error, keep in $hasFlash*/
		$hasFlash = ($session->getFlash('error')) ? $hasFlash = $session->getFlash('error') : array();
		/*Clear all session data*/
		$session->destroy();
		/*If $hasFlash not empty, setFlash again*/
		if (!empty($hasFlash))	{	$session->setFlash('error',$hasFlash);	}

		/*If form submitted from modal screen, save the product_id  and quantity in session*/
		if (Yii::$app->request->post())
		{
			$orders = new Orders();
			if ($orders->load(Yii::$app->request->post()) && $orders->validate(['product_id','quantity']))
			{
				$session->set('product_id',$orders->product_id);
				$session->set('quantity',$orders->quantity);
				/*Redirect to check-out page*/
				$this->redirect(Url::toRoute(['shop/check-out']));
			}
			
			
		}
		/*Otherwise show the default product listing page*/
		$this->layout = 'shop';
		$pageTitle = 'Product List - Simple Shopping App';
		$all_products = Products::find()->all();
        return $this->render('index',['title'=>$pageTitle, 'products'=>$all_products]);
    }
	
	/**
	 * Display the selected product in the modal window
	**/
	public function actionShowProduct($id = null)
	{

		$productInfo = Products::findOne($id);
		
		if (isset($productInfo) && Yii::$app->request->isAjax)	
		{
			
			$model = new Orders();
			$model->product_id = $id;
			return $this->renderAjax('product_form',['model'=>$model]);
			
		}	else	{
			$session = Yii::$app->Session;
			$session->setFlash('error', "Please follow the proper flow of the shopping app!");
			$this->goHome();
			
		}
		
	}
	
	/*For Testing*/
	public function actionTest()
	{
		$session = Yii::$app->Session;
		return $session->get('product_id').' '.$session->get('quantity');
		//$this->renderAjax('test');
	}
	
	/**
	 * Check Out Screen to display selected Product and Quantity
	**/
	public function actionCheckOut()
	{
		$session = Yii::$app->Session;
		/*Check session, if no product_id and quantity, redirect back to index page*/
		
		if ($session->get('product_id') !== null && $session->get('quantity') !==null)
		{
			$model = new Orders();
			$model->product_id = $session->get('product_id');
			$model->quantity = $session->get('quantity');
			$model->delivery_country = $session->get('delivery_country');
			$model->discount_code = $session->get('discount_code');
			
			/*Get Total Price*/
			
			$getTotalPrice = ($this->GetTotalPrice()) ? $this->GetTotalPrice() : $this->redirect(Url::toRoute('/'));;
			$model->priceInfo = $getTotalPrice;
			$model->total_price = $getTotalPrice['Total'];
			$model->unit_price = $model->product->selling_price;

			/*If submitted from check-out page. and validated passed*/
			
			if ($model->load(Yii::$app->request->post()) && $model->validate())
			{
				//$session->setFlash('error', "Saved");
				//$model->save();
				/*
					suppose to save the order into database, then redirect to summary page
					but i will just store it into session for this Testing purpose
				*/
				/*Not going to get order from database, will get from session this time*/
				/*Generate a random order number for this testing, suppose to get from database*/
				$myordernumber = $this->generateRandomOrderNumber(6);
					
				$dataToBeSaved = array(
					'orderNumber' => 'ORD'.$myordernumber,
					'product_id' => $model->product_id,
					'product_idName' => $model->product->product_name,
					'brand' => $model->product->brand,
					'quantity' => $model->quantity,
					'delivery_country' => $model->delivery_country,
					'delivery_countryName' => $model->countryName,
					'discount_code' => $model->discount_code,
					'unit_price' => $model->unit_price,
					'total_price' => $model->total_price,
					'priceInfo' => $model->priceInfo
				
				);
				
				$session->set('FinalSummary',$dataToBeSaved);
				$this->redirect(Url::toRoute('shop/summary'));
				
			}	

			
			$this->layout = 'shop';
			$pageTitle = 'Check Out - Simple Shopping App';
			return $this->render('check_out',['title'=>$pageTitle, 'model' => $model]);
			
		}	else	{
			$session->setFlash('error', "Please follow the proper flow of the shopping app!");
			$this->goHome();
		}
		

	}
	
	private function generateRandomOrderNumber($length)
	{
		$rNumber = '';
		for ($i = 0; $i<$length; $i++)
		{
			$rNumber .=  (rand(0,9));
		}
		return $rNumber;
	} 
	
	/**
	 * Check Out Screen to display selected Product and Quantity
	**/
	public function actionSummary()
	{
		$session = Yii::$app->Session;
		
		/*If session cannot be found, redirect to home with error*/
		if($session->get('FinalSummary') !== null)
		{
			$this->layout = 'shop';
			$pageTitle = 'Summary - Simple Shopping App';
			
			return $this->render('summary',['title'=>$pageTitle, 'myorder'=>$session->get('FinalSummary')]);
			
		} else	{
			$session->setFlash('error', "Please follow the proper flow of the shopping app!");
			$this->goHome();
		}
	}
	/*
	 * Return total price
	*/
	private function GetTotalPrice()
	{
		$session = Yii::$app->Session;

		$s_deliver_country = $session->get('delivery_country');
		$s_discount_code = $session->get('discount_code');
		
		/*Check session, if no product_id and quantity, redirect back to index page*/
		if ($session->get('product_id') !== null && $session->get('quantity') !== null)
		{
			/*assign session value into model*/
			$orders = new Orders();
			$orders->product_id = $session->get('product_id');
			$orders->quantity = $session->get('quantity');
			$orders->delivery_country = $session->get('delivery_country');
			$orders->discount_code = $session->get('discount_code');
			
			
			$unitPrice = $orders->product->selling_price;
			$quantity = $orders->quantity;
			/*Calculate the total Product rice*/
			$price['ItemOnly'] = $unitPrice * $quantity;
			
			$price['ShipFee'] = 0;
			
			/*Calculate the Shipping Fee*/
			switch ( $orders->countryName)
			{
				case 'Malaysia':
					$price['ShipFee'] = ($quantity >= 2 || $price['ItemOnly'] > 150) ? 0 : 10;
					break;
					
				case 'Singapore':
					$price['ShipFee'] = ($price['ItemOnly'] > 300) ? 0 : 20;
					break;
				
				case 'Brunei':
					$price['ShipFee'] = ($price['ItemOnly'] > 300) ? 0 : 25;
					break;
				default:
				case '':
					$price['ShipFee'] = 0;
					break;
			}
			
			/*Calculate the Discount*/
			$price['Discount'] = 0;
			if ($session->get('discount_code'))	{
				switch ($session->get('discount_code'))
				{
					case 'GIVEME15':
						//$price['Discount'] =  15 ;
						/*Validate 1 more time in case user submit improper*/
						$price['Discount'] = ($price['ItemOnly'] >= 100) ? 15 : 0;
						break;
					
					case 'OFF5PC':
						//$price['Discount'] = $price['ItemOnly']*(5/100) ;
						/*Validate 1 more time in case user submit improper*/
						$price['Discount'] = ($quantity >= 2)? $price['ItemOnly']*(5/100) : 0 ;
						break;
					default:
					case '':
						$price['Discount'] = 0;
						break;
				}
			}
			
			/*Calculate Final Total*/
			$price['Total'] = $price['ItemOnly'] - $price['Discount'] + $price['ShipFee'];
			
			return $price;
		
		}	else	{
			
			return false;
			
		}
	}
	
	/*
	 * Get Price via Ajax
	*/
	public function actionAjaxGetTotalPrice()
	{
		if (Yii::$app->request->isAjax)
		{
		Yii::$app->response->format = Response::FORMAT_JSON;
		
		$session = Yii::$app->Session;
		$orders = new Orders;
		$orders->product_id = $session->get('product_id');
		$orders->quantity = $session->get('quantity');
		
		$orders->load(Yii::$app->request->post());
		$orders->validate();
		
		/*Save the latest delivery_country into session if no error*/
		if (empty($orders->getErrors('delivery_country')))
		{
			$session->set('delivery_country',$orders->delivery_country);
		}
		
		/*Save the latest discount_code into session if no error*/
		if (empty($orders->getErrors('discount_code')))
		{
			$session->set('discount_code',$orders->discount_code);
		}
		
		
		$price = $this->GetTotalPrice();

		/**Build Price from GetTotalPrice and formate as currencyt**/
		foreach ($price as $key=>$value)
		{
			$ajaxPrice[$key] = Html::encode(\Yii::$app->formatter->asCurrency($value));
		}
		
		$response['Price'] = $ajaxPrice;
		
		/**Build Error Message from validated Error**/
		foreach ($orders->getErrors() as $attribute => $errors) {
            $Error[yii\helpers\Html::getInputId($orders, $attribute)] = $errors;
        }
		
		$response['Error'] = $Error;
		
		
		return $response;
		}	else	{
			$session->setFlash('error', "Please follow the proper flow of the shopping app!");
			$this->goHome();
		}
		
	}
}